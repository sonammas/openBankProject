package com.sonam.mas.iotproducer.controller;

import com.sonam.mas.iotproducer.model.IotDevice;
import com.sonam.mas.iotproducer.producer.IotProducerService2;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * This rest controller is used to publish any IotDevice data to kafka
 */
@RestController
@RequestMapping(value = "iotdevice")
public class IotCommandController {

    private final IotProducerService2 iotProducerService;

    public IotCommandController(IotProducerService2 iotProducerService) {
        this.iotProducerService = iotProducerService;
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean publishIotDeviceData(@RequestBody IotDevice iotDevice) {
        return iotProducerService.publish(iotDevice);
    }
}
