package controller;

import lombok.AllArgsConstructor;
import model.Transaction;
import org.springframework.web.bind.annotation.*;
import service.MonetaryTransactionsService;

import java.util.List;

@RestController
@RequestMapping(value = "openbank")
@AllArgsConstructor
public class OpenBankRestController {

    private final MonetaryTransactionsService monetaryTransactionsService;

    // Case 1 : Get transaction List
    @GetMapping(value = "{id}/transactions/{accountId}")
    public List<Transaction> getTransactionList(@PathVariable("id") final long id,
                                                @PathVariable("accountId") final Long accountId) {
        return monetaryTransactionsService.fetchTransactionList(id, accountId);
    }
}
