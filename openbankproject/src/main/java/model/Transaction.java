package model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author sonam.masuriha
 * Check https://github.com/OpenBankProject/OBP-API/wiki/REST-API-V1.2.1#transactions
 */

@Data
public class Transaction {

    public static final String ISO8601_TIMESTAMP_FORMAT = "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'";

    private String id;

    @JsonProperty("this_account")
    private Account ownAccount;

    @JsonProperty("other_account")
    private Account targetAccount;

    private Details details;

    private Metadata metadata;

    @Data
    private class Details {

        private String type;

        private String description;

        @JsonProperty("new_balance")
        private Money newBalance;

        @JsonProperty("value")
        private Money value;
    }

    @Data
    public class Metadata {
        @JsonProperty("image_URL")
        private Object counterPartyLogoPath;
    }
}
