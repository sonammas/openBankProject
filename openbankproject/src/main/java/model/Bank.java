package model;

import lombok.Data;

@Data
public class Bank {

    private String id;

    private String shortName;

}
