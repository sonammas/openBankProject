package model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
public class Account {

    private String id;

    @JsonProperty("bank_id")
    private String bankId;

    private String number;

    private Bank bank;

    private Holders holder;

    @JsonProperty("IBAN")
    private String iban;

    @JsonProperty("swift_bic")
    private String bic;

}
