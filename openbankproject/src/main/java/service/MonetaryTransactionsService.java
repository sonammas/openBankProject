package service;

import lombok.Data;
import model.Transaction;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class MonetaryTransactionsService {

    @Value("${obp.api.versionedUrl}")
    private String apiUrl;

    private RestTemplate restTemplate = new RestTemplate();

    public List<Transaction> fetchTransactionList(final Long bankId, final Long accountId) {
        //TODO implement authentication service
        String token = null;
        String allTransactionsUrl = String.format("%s/banks/%s/accounts/%s/owner/transactions", apiUrl,
                bankId, accountId);
        HttpEntity<Void> req = prepareAuthRequest(token);
        Transactions transactions = restTemplate.exchange(allTransactionsUrl, HttpMethod.GET, req, Transactions.class)
                .getBody();
        return transactions.getTransactions();
    }

    private HttpEntity<Void> prepareAuthRequest(String token) {
        HttpHeaders authHeaders = new HttpHeaders();
        String dlHeader = String.format("DirectLogin token=%s", token);
        authHeaders.add(HttpHeaders.AUTHORIZATION, dlHeader);
        return new HttpEntity<>(null, authHeaders);
    }

    @Data
    private static class Transactions {
        private List<Transaction> transactions;
    }
}
