package com.mas.sonam.iotapplication.service;

import com.mas.sonam.iotapplication.model.IotDevice;
import com.mas.sonam.iotapplication.repository.IotDeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This service will save the iotDevice data to the mongoDB as a document
 */
@Service
public class IotSaveKafkaMessageService {

    @Autowired
    private final IotDeviceRepository iotDeviceRepository;

    public IotSaveKafkaMessageService(IotDeviceRepository iotDeviceRepository) {
        this.iotDeviceRepository = iotDeviceRepository;
    }

    public void saveKafkaMessageToDatabase(final IotDevice iotDevice) {
        iotDeviceRepository.save(iotDevice);
    }
}
